var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  if(kanal.charAt(0)!="\""){
    this.socket.emit('pridruzitevZahteva', {
      novKanal: kanal
    });
    return false;
  }else{
    var besede = kanal.split(" ");
    if(besede[0].charAt(0)!="\"")
      return "Neznan ukaz1";
    if(besede[0].charAt(besede[0].length - 1)!="\"")
      return "Neznan ukaz2";
    var ime = besede[0].substring(1,besede[0].length - 1);
    besede.shift();
    var str = besede.join(" ");
    if(str.charAt(0)!="\"")
      return "Neznan ukaz3";
    if(str.charAt(besede[0].length - 1)!="\"")
      return "Neznan ukaz4";
    str = str.substring(1,str.length - 1);
    this.socket.emit('pridruzitevZahteva', {
      novKanal: ime,
      geslo: str
    });
  }
};


Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      sporocilo = this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      if(besede.length <= 1){
        sporocilo = 'Neznan ukaz.';
        break;
      }
      if(besede[0].charAt(0) != "\""){
        sporocilo = 'Neznan ukaz.';
        break;
      }
      if(besede[0].charAt(besede[0].length - 1) != "\""){
        sporocilo = 'Neznan ukaz.';
        break;
      }
      var prejemnik = besede[0].substring(1, besede[0].length - 1);
      besede.shift();
      
      var besedilo = besede.join(" ");
      
      if(besedilo.charAt(0) != "\""){
        sporocilo = 'Neznan ukaz.';
        break;
      }
      if(besedilo.charAt(besedilo.length - 1) != "\""){
        sporocilo = 'Neznan ukaz.';
        break;
      }
      besedilo = besedilo.substring(1, besedilo.length - 1);
      this.socket.emit('private', {
        za: prejemnik,
        tekst: besedilo
      });
      sporocilo = "(Zasebno za "+prejemnik+"): "+besedilo;
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};