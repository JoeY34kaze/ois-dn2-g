var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var besede;
var kanaliZasedenost = {};
var kanaliGesla = {};


exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, {novKanal:'Skedenj'});
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZasebna(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal1) {
  var kanal = kanal1.novKanal;
  if (typeof kanal1.geslo === 'undefined') {
    if (typeof kanaliZasedenost[kanal] === 'undefined') {
      kanaliZasedenost[kanal] = 1;
      kanaliGesla[kanal] = "";
    }else if(kanaliZasedenost[kanal]==0){
      kanaliGesla[kanal] = "";
      kanaliZasedenost[kanal] = 1;
    }else{
      if (kanaliGesla[kanal] == '') 
        kanaliZasedenost[kanal]++;
      else
        return "Kanal "+kanal+" je zasciten z geslom.";
    }
  } else{
    if (typeof kanaliZasedenost[kanal] === 'undefined') {
      kanaliZasedenost[kanal] = 1;
      kanaliGesla[kanal] = kanal1.geslo;
    }else if(kanaliZasedenost[kanal] == 0){
      kanaliZasedenost[kanal] = 1;
      kanaliGesla[kanal] = kanal1.geslo;
    }else{
      if(kanaliGesla[kanal] == "")
        return "Izbrani kanal "+kanal+" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom"
      if(kanaliGesla[kanal] != kanal1.geslo)
        return "Pridružitev v kanal "+kanal+" ni bilo uspešno, ker je geslo napačno!"
    }
  }
  
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: divElementEnostavniTekst(vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.')
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var pod = [];
  var st = 0;
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
          
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        pod[st++] = vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    pod[st++] = vzdevkiGledeNaSocket[socket.id];
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
    socket.emit('uporabniki', {besedilo: pod});
    socket.broadcast.to(trenutniKanal[socket.id]).emit('uporabniki', {besedilo: pod});
  }else{
    pod[0] = vzdevkiGledeNaSocket[socket.id];
    socket.broadcast.to(trenutniKanal[socket.id]).emit('uporabniki', {besedilo: pod});
    socket.emit('uporabniki', {besedilo: pod});
  }
  
  return false;
}
function obdelajZasebna(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki){
  socket.on('private', function (sporocilo) {
    //console.log("Zasebno sporocilo za " + sporocilo.za + ": "+sporocilo.tekst);
    var socketID;
    var found = false;
    for (var key in vzdevkiGledeNaSocket) {
      if(vzdevkiGledeNaSocket[key].localeCompare(sporocilo.za)==0 && key != socket.id){
        socketID = key;
        found = true;
        break;
      }
    }
    if(found){
      
      var tekst = zakodiraj(sporocilo.tekst);
      for(var i = 0;i<besede.length;i++)
        tekst = tekst.replace(RegExp('\\b'+besede[i]+'\\b','gi') , zvezdice(besede[i].length));
      
      tekst = tekst.replace(/;\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">');
      tekst = tekst.replace(/:\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
      tekst = tekst.replace(/\(y\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">');
      tekst = tekst.replace(/:\*/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">');
      tekst = tekst.replace(/:\(/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">');
      
      io.sockets.socket(socketID).emit('sporocilo',{
        besedilo: divElementEnostavniTekst(vzdevkiGledeNaSocket[socket.id] + ": " +tekst)
      });
    }else{
      socket.emit('sporocilo', {besedilo: "Sporočilo "+sporocilo.tekst+" uporabniku z vzdevkom "+sporocilo.za+" ni bilo mogoče posredovati."});
    }
  });
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        
        var kanal = trenutniKanal[socket.id];
        var uporabnikiNaKanalu = io.sockets.clients(kanal);
        var pod = [];
        var st = 0;
        if (uporabnikiNaKanalu.length > 1) {
          for (var i in uporabnikiNaKanalu) {
            var uporabnikSocketId = uporabnikiNaKanalu[i].id;
            if (uporabnikSocketId != socket.id) {
              pod[st++] = vzdevkiGledeNaSocket[uporabnikSocketId];
            }
          }
          pod[st++] = vzdevkiGledeNaSocket[socket.id];
          socket.broadcast.to(trenutniKanal[socket.id]).emit('uporabniki', {
            besedilo: pod
          });
          socket.emit('uporabniki', {besedilo: pod});
        }
        
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}


function zakodiraj(str){
  str = str.replace(/&/g, "&amp;");
  str = str.replace(/>/g, "&gt;");
  str = str.replace(/</g, "&lt;");
  str = str.replace(/"/g, "&quot;");
  str = str.replace(/'/g, "&#039;");
  return str;
}

function divElementEnostavniTekst(sporocilo) {
  return '<div style="font-weight: bold">'+sporocilo+'</div>';
}

function zvezdice(st) {
  var str = ""
  for(var i = 0;i<st;i++)
    str += "*";
  return str;  
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {

    var tekst = zakodiraj(sporocilo.besedilo);
    
    for(var i = 0;i<besede.length;i++)
      tekst = tekst.replace(RegExp('\\b'+besede[i]+'\\b','gi') , zvezdice(besede[i].length));
    
    tekst = tekst.replace(/;\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">');
    tekst = tekst.replace(/:\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
    tekst = tekst.replace(/\(y\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">');
    tekst = tekst.replace(/:\*/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">');
    tekst = tekst.replace(/:\(/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">');
    
    //tekst = divElementEnostavniTekst(tekst);
    socket.emit('sporocilo', {//poslje nazaj posiljatelju
      besedilo: divElementEnostavniTekst(tekst)
    });
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {//poslje vsem ostalim na kanalu
      besedilo: divElementEnostavniTekst(vzdevkiGledeNaSocket[socket.id]+": "+tekst)
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal1) {
  
  var kanal = trenutniKanal[socket.id];
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var pod = [];
  var st = 0;
  if (uporabnikiNaKanalu.length > 1) {
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        pod[st++] = vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    //pod[st++] = vzdevkiGledeNaSocket[socket.id];
    socket.broadcast.to(trenutniKanal[socket.id]).emit('uporabniki', {
      besedilo: pod
    });
    socket.emit('uporabniki', {besedilo: pod});
  }
    
    
    var tre = trenutniKanal[socket.id]
    kanaliZasedenost[tre]--;
    socket.leave(tre);
    var msg = pridruzitevKanalu(socket, kanal1);
    if(msg != false){
      kanaliZasedenost[tre]++;
      socket.join(tre);
      socket.emit('sporocilo', {
        besedilo: msg
      });
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    //console.log("Odjavil se je: " + uporabljeniVzdevki[vzdevekIndeks]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });

}

var fs  = require('fs');
    fs.exists("./lib/swearWords.txt", function(datotekaObstaja) {
      if (datotekaObstaja) {
        //console.log("Dattekaobstaja");
        fs.readFile("./lib/swearWords.txt", function(napaka, datotekaVsebina) {
          if (napaka) {
            console.log("Napaka");
          } else {
            besede = datotekaVsebina.toString('utf8').split("\r\n");
          }
        });
      }
    });

